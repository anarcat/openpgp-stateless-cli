#!/usr/bin/make -f

# dependencies:

# apt install weasyprint xml2rfc ruby-kramdown-rfc2629

draft = draft-openpgp-stateless-cli
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf

all: $(OUTPUT)

%.xmlv2: sop.md
	kramdown-rfc2629 < $< > $@.tmp
	mv $@.tmp $@

%.xml: %.xmlv2
	xml2rfc --v2v3 $< -o $@

%.html: %.xml
	xml2rfc $< --html -o $@

%.txt: %.xml
	xml2rfc $< --text -o $@

%.pdf: %.xml
	xml2rfc $< --pdf -o $@

clean:
	-rm -rf $(OUTPUT) $(draft).xmlv2 .refcache/ metadata.min.js
.PRECIOUS: $(draft).xmlv2
.PHONY: clean all
